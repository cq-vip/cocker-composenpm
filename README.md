# docker-compose的nginx，php，mysql环境
使用docker-composed构建了nginx，php，mysql环境
## 详细目录
```
├── nginx
|   └──nginx.conf #nginx配置文件
|   └──conf.d:
|      └──xxx.xxx.com.conf #自己的域名配置文件
├── php:
|   └──Dockerfile  #php的cockerFile 指定了镜像以及初始化的命令
|   └── php.ini #php配置文件
|   └── php-fpm.conf #php-fpm的文件
├── mysql:
|   └──data #mysql的数据文件
|   └──init
|       └──init.sql #初始化文件
|   └──my.conf#mysql的配置文件
├── src #代码文件，里面为thinkphp6rc4的代码
├── docker-compose.yml
  -
```